# Date Range Filter Extension

## Deployment

The code behind all Tableau extensions must be hosted by a web server.  To simplify deployment for this extension, the code is being hosted in GitLab at the following link:
```
https://tableau-extensions.gitlab.io/daterangefilter/
```

This means you can just download the *.trex* file, and add it to your dashboard by using the Extension button.

![Dashboard Menu](screenshots/DashboardMenu.png)

If you wish to modify the extension, you can download the full source code under the public directory.  Make whatever changes are desired, and the host the website on your own web server.  If changes are made, make sure to update the *.trex* file's source-location.url attribute to point to your web server. 

