tableau.extensions.initializeAsync().then(() => {
  console.log('Tableau Extension Library loaded');
  
  //  Define the worksheet and field to filter
  let sheetName = "Timeline",
      fieldName = "Date",
      minDateField = "Min Date",
      maxDateField = "Max Date";
  
  //  Define the menu selector
  let menu = 'input[name="daterange"]';
  
  //  Define a flag, so that we know if the filter was changed by our customer filter or the dashboard
  let wasCustomFilter = false;
  
  //  Placeholder for the original min/max dates
  let sheetMinDate,
      sheetMaxDate;
  
  //  Define the function for setting a date range
  function updateFilterRange(sheet, field, fromDate, endDate){
    
    //  Update the worksheet's filter
    if (wasCustomFilter) {
      let newRange = {min:fromDate, max: endDate};
      sheet.applyRangeFilterAsync(field, newRange);
    }
    
    //  Reset the flag
    wasCustomFilter = false;
  }
  
  //  Define function handler for when a date range is selected
  function dateSelected(event, picker){

    //  Pass on the selection from the UI to Tableau filter
    wasCustomFilter = true;
    updateFilterRange(sheet,fieldName,picker.startDate._d, picker.endDate._d);
  }
  
  //  Define a function to updating just the UI of the custom filter
  function updateFilterUI(event){

    //  Get the filter
    event.getFilterAsync().then( response => {
      
      //  Init variables for the new min/max dates
      let min = sheetMinDate,
          max = sheetMaxDate;
      
      //  Make sure there was an actual selection
      if (response.appliedValues && response.appliedValues.length>0) {
        
        //  Sort the selected marks
        let sortedValues = response.appliedValues.sort(function(a,b){ return a._value>b._value });
        
        //  Figure out the min/max values
        min = new Date(sortedValues[0].formattedValue);
        max = new Date(sortedValues[sortedValues.length-1].formattedValue);
        
      } else if (response.maxValue){
        
        //  Just a regular date range
        min = new Date(response.minValue.formattedValue);
        max = new Date(response.maxValue.formattedValue);
      }
      
      //  Remove the event handler for the custom filter
      $(menu).off('apply.daterangepicker', dateSelected);

      //  Make the selections for start/end dates
      $(menu).data('daterangepicker').setStartDate(min);
      $(menu).data('daterangepicker').setEndDate(max);

      //  Return the event handler for future selections
      $(menu).on('apply.daterangepicker', dateSelected);
    })
  }
  
  //  Get a reference to one of the sheets
  let dashboard = tableau.extensions.dashboardContent.dashboard,
    sheet = dashboard.worksheets.find(w=>w.name === sheetName);
  
  //  Get the existing date range
  sheet.getSummaryDataAsync().then( table => {
    
    //  Get the max date
    let maxDateColumn = table.columns.find(columnNames => columnNames.fieldName === maxDateField).index;
    sheetMaxDate = new Date(table.data[0][maxDateColumn].value);
    
    //  Get the min date
    let minDateColumn = table.columns.find(columnNames => columnNames.fieldName === minDateField).index;
    sheetMinDate = new Date(table.data[0][minDateColumn].value);
    
    //  Init the tableau filter
    updateFilterRange(sheet, fieldName, sheetMinDate, sheetMaxDate)
    
    //  Configure the settings for the date picker
    let options = {
      opens: 'left',
      startDate: sheetMinDate,
      endDate: sheetMaxDate,
    };
    
    //  Initialize the JS date picker
    $(menu).daterangepicker(options);
    $(menu).on('apply.daterangepicker', dateSelected);
    
    //  Set event handler, in case the filter gets reset from another part of the dashboard
    sheet.addEventListener(tableau.TableauEventType.FilterChanged, updateFilterUI)
    
  })
});